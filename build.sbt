name := """lightest-minder-bridge"""
organization := "gov.tubitak.eu"

version := "1.0"

lazy val lightestMinderBridge = (project in file(".")).enablePlugins(PlayScala)

resolvers += Resolver.mavenLocal

resolvers += "Eid public repository" at "http://mindertestbed.org:8081/nexus/content/groups/public/"

resolvers += "Bintray Plugin Releases" at "https://dl.bintray.com/sbt/sbt-plugin-releases/"

resolvers += Resolver.sbtPluginRepo("releases")

scalaVersion := "2.12.3"

libraryDependencies ++= Seq(
  guice,
  "gov.tubitak.minder" % "minder-common" % "1.0.0",
  "gov.tubitak.minder" % "minder-client" % "1.0.1",
  "gov.tubitak.eu" % "lightest-minder-common" % "1.1.4",
  "com.adrianhurt" %% "play-bootstrap" % "1.2-P26-B4",
  "commons-io" % "commons-io" % "2.6",
  "org.webjars.bower" % "bootstrap-sass" % "3.3.6",
  //"org.webjars" % "bootstrap-sass" % "3.3.7" exclude("org.webjars", "jquery"),
  "org.webjars" % "jquery" % "3.3.1",
  "org.webjars" % "font-awesome" % "4.7.0",
  "org.webjars.bower" % "compass-mixins" % "0.12.7",
  "org.scalatestplus.play" %% "scalatestplus-play" % "3.1.2" % Test
)
