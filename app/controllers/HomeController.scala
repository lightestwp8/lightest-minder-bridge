

import javax.inject._

import play.api.mvc._
import gov.tubitak.eu.lightest.minderadapters._

package controllers {

  import java.net.{DatagramPacket, DatagramSocket, InetSocketAddress}

  /**
    * This controller creates an `Action` to handle HTTP requests to the
    * application's home page.
    */
  @Singleton
  class HomeController @Inject()(implicit cc: ControllerComponents, adapterContainer: MinderAdapterContainer) extends AbstractController(cc) {

    /**
      * Create an Action to render an HTML page.
      *
      * The configuration in the `routes` file means that this method
      * will be called when the application receives a `GET` request with
      * a path of `/`.
      */
    def index() = Action { implicit request: Request[AnyContent] =>
      Ok(views.html.index())
    }
  }
}
