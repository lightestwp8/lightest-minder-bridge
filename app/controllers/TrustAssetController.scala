import javax.inject.Inject

import gov.tubitak.eu.lightest.minderadapters._
import org.apache.commons.io.IOUtils
import play.api.mvc.{AnyContent, Request, _}

package controllers {

  import javax.annotation.Nonnull

  import gov.tubitak.eu.lightest.BridgeCommonConfig
  import play.Logger

  /**
    * @author yerlibilgin
    */
  class TrustAssetController @Inject()(cc: ControllerComponents, adapterContainer: MinderAdapterContainer) extends AbstractController(cc) {

    val LOGGER = Logger.of(classOf[TrustAssetController])

    def downloadTrustXml(file: String) = Action { implicit request: Request[AnyContent] =>
      LOGGER.debug(s"trust asset $file was requested from the bridge")

      if (BridgeCommonConfig.isTrustAssetRequestForwarded) {
        LOGGER.debug(s"the trust asset request for $file will be forwarded to Minder")

        //forward this request to Minder as the 'asset download signal'
        adapterContainer.atvAdapter.testAssetRequested(file)


        //create a waiter object within this context, to make sure that we don't
        //deal with Mapping the correct object to the correct response.
        val trustResourceResponseWaiter = new Array[Object](1)

        // create a new listener object within this thread and register it to the MINDER adapter
        val slotListener = createInlineSlotListener(trustResourceResponseWaiter)

        adapterContainer.atvAdapter.registerATVSlotListener(slotListener)

        //wait for the resource to be available

        try {
          trustResourceResponseWaiter.synchronized {
            trustResourceResponseWaiter.wait(BridgeCommonConfig.getTrustAssetRequestWaitTimeout())
          }

          if (trustResourceResponseWaiter(0) != null) {
            LOGGER.error(s"Received the asset $file from Minder. Initiate download")
            val bytes = trustResourceResponseWaiter(0).asInstanceOf[Array[Byte]]
            Ok(bytes).as("application/x-download").withHeaders(("Content-disposition", s"attachment; filename=$file"))
          } else {
            LOGGER.error(s"Couldn't get the asset $file from Minder. Timed out.")
            InternalServerError(s"Couldn't get the asset $file from Minder. Timed out.")
          }
        } catch {
          case ex: InterruptedException => {
            LOGGER.error(s"Wait for object was interrupted")
            InternalServerError("Couldn't get the resource from Minder")
          }
        } finally {
          //no matter what happens, remove me from the listeners
          adapterContainer.atvAdapter.removeATVSlotListener(slotListener)

          //FIXME: the slot call may be confused. need to add the name of the asset
          //to the slot as well
        }
      } else {
        LOGGER.debug(s"the trust asset request for $file will be sought for in the /trust/ resource directory")
        val stream = this.getClass.getResourceAsStream("/trust/" + file)
        if (stream == null) {
          LOGGER.warn(s"trust asset $file was not found")
          BadRequest(s"a resource with name $file not found")
        } else {
          LOGGER.warn(s"trust asset $file found,. Initiate download")
          var bytes = IOUtils.toByteArray(stream)
          Ok(bytes).as("application/x-download").withHeaders(("Content-disposition", s"attachment; filename=$file"))
        }
      }
    }

    /**
      * Create a listener that fills the waiter object and notifies the waiting thread
      *
      * @param waiterObject
      */
    def createInlineSlotListener(@Nonnull waiterObject: Array[Object]): IATVSlotListener = {
      LOGGER.debug("Create a slot listener")
      if (waiterObject == null)
        throw new NullPointerException("Waiter object cannot be null")

      new IATVSlotListener {
        /**
          * Please see {@link gov.tubitak.eu.lightest.minderadapters.ATVAdapter#sendTestAssetResponse}
          *
          * @param assetData
          */
        def sendTestAssetResponse(assetData: Array[Byte]): Unit = {
          waiterObject.synchronized {
            //fill the waiter object
            waiterObject(0) = assetData
            //notify
            waiterObject.notify()
          }
        }

        /**
          * Please see {@link gov.tubitak.eu.lightest.minderadapters.ATVAdapter#sendDatagramResponse}
          *
          * @param datagramPacket
          */
        def sendDatagramResponse(datagramPacket: Array[Byte]): Unit = {
          //ignore
        }
      }
    }
  }

}
