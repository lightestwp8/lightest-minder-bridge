import javax.inject.Inject

import gov.tubitak.eu.lightest.minderadapters._
import play.api.mvc._

package controllers {
  import gov.tubitak.eu.common.TestResult
  import play.Logger
  /**
    * @author yerlibilgin
    */
  class TestStatusController @Inject()(cc: ControllerComponents, adapterContainer: MinderAdapterContainer) extends AbstractController(cc) {

    val LOGGER = Logger.of(classOf[TestStatusController])

    def testFinished(success: Boolean, result: String) = Action { implicit request: Request[AnyContent] =>
      LOGGER.debug("Call the reset signal")
      val tr = new TestResult
      tr.setSuccess(success)
      tr.setResultData(result.getBytes)
      adapterContainer.atvEndUserAdapter.testFinished(tr)
      Ok("Ok")
    }
  }

}
