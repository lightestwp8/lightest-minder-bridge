package gov.tubitak.eu.lightest.minderadapters

import java.util.Properties
import javax.inject.{Inject, Singleton}

import gov.tubitak.minder.client.MinderClient
import org.apache.log4j.PropertyConfigurator
import play.{Environment, Logger}

/**
  *
  */
@Singleton
class MinderAdapterContainer @Inject()(environment: Environment) {
  val LOGGER = Logger.of(classOf[MinderAdapterContainer])

  /**
    * The Automated Trust Verifier adapter, initialized via Minder Framework, never initialize manually
    */
  var atvAdapter: ATVAdapter = null;

  /**
    * The Automated Trust Verifier adapter, initialized via Minder Framework, never initialize manually
    */
  var atvEndUserAdapter: ATVEndUserAdapter = null;

  def init(): Unit = {
    Logger.debug("Initialize minder adapters")
    //initialize ATV adapter
    PropertyConfigurator.configure(this.getClass.getResource("/log4j.properties"));

    {
      try {
        var properties = new Properties()
        properties.load(this.getClass.getResourceAsStream("/bridgeConf/atv-adapter.properties"))
        var minderClient = new MinderClient(properties, environment.classLoader())
        atvAdapter = minderClient.adapter.asInstanceOf[ATVAdapter]
      } catch {
        case _: Throwable => {
          atvAdapter = null;
        }
      }
    }

    //initialize ATV END USER Adapter
    {
      try {
        var properties = new Properties()
        properties.load(this.getClass.getResourceAsStream("/bridgeConf/atv-enduser-adapter.properties"))
        var minderClient = new MinderClient(properties, environment.classLoader())
        atvEndUserAdapter = minderClient.adapter.asInstanceOf[ATVEndUserAdapter]
      } catch {
        case th: Throwable => {
          atvEndUserAdapter = null;
        }
      }
    }
  }


  //call init
  init()
}
