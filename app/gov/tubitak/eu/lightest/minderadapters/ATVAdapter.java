package gov.tubitak.eu.lightest.minderadapters;

import minderengine.Adapter;
import minderengine.Signal;
import minderengine.Slot;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.List;

/**
 * @author: myildiz
 * @date: 26.02.2018.
 */
public abstract class ATVAdapter extends LIGHTestMinderAdapter {


  //<editor-fold desc="ATV SLOT LISTENER Registration">

  private List<IATVSlotListener> slotListeners = new ArrayList<>();

  public void registerATVSlotListener(IATVSlotListener listener){
    this.slotListeners.add(listener);
  }

  public void removeATVSlotListener(IATVSlotListener listener){
    this.slotListeners.remove(listener);
  }

  //</editor-fold>


  /**
   * Send a signal to Minder informing that an asset request for a specific asset was issued.
   * The response of this signal is the slot {@link ATVAdapter#sendTestAssetResponse}
   *
   * @param trustXmlAssetName the name of the test asset to be provided back as trust xml. Never null
   */
  @Signal
  public abstract void testAssetRequested(@Nonnull String trustXmlAssetName);


  /**
   * The slot is called by Minder after a request via {@link ATVAdapter#testAssetRequested} is issued. The asset with the same name is provided back using this slot
   *
   * @param assetData actual asset data as byte array
   */
  @Slot
  public void sendTestAssetResponse(@Nonnull byte[] assetData) {

    for(IATVSlotListener listener : slotListeners){
      listener.sendTestAssetResponse(assetData);
    }
  }


  /**
   * Issue a datagram packet to Minder
   * @param datagramPacket the contents of the datagram packet
   */
  @Signal
  public abstract void datagramRequestReceived(byte[] datagramPacket);

  /**
   * Receive a datagram packet (usually a DNS response) from Minder, and distribute it to the listeners
   * @param datagramPacket
   */
  @Slot
  public void sendDatagramResponse(byte []datagramPacket) {
    for(IATVSlotListener listener : slotListeners){
      listener.sendDatagramResponse(datagramPacket);
    }
  }

}
