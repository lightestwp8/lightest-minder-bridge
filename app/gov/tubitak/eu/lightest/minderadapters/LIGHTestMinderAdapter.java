package gov.tubitak.eu.lightest.minderadapters;

import minderengine.Adapter;
import minderengine.SUTIdentifier;
import minderengine.SUTIdentifiers;

/**
 * @author: myildiz
 * @date: 1.03.2018.
 */
public abstract class LIGHTestMinderAdapter extends Adapter {

  private final SUTIdentifiers sutIdentifiers;

  LIGHTestMinderAdapter() {
    sutIdentifiers = new SUTIdentifiers();
    SUTIdentifier identifier = new SUTIdentifier();
    identifier.setSutName("LIGHTest ATV");
    sutIdentifiers.getIdentifiers().add(identifier);
  }

  @Override
  public SUTIdentifiers getSUTIdentifiers() {
    return sutIdentifiers;
  }
}
