package gov.tubitak.eu.lightest.minderadapters;

import gov.tubitak.eu.common.InitialTestData;

/**
 * @author: myildiz
 * @date: 28.02.2018.
 */
public interface IATVEndUserSlotListener {
  /**
   * Please see {@link gov.tubitak.eu.lightest.minderadapters.ATVEndUserAdapter#triggerTest}
   * @param initialTestData
   */
  void triggerTest(InitialTestData initialTestData);

}
