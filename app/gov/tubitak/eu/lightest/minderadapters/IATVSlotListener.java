package gov.tubitak.eu.lightest.minderadapters;

/**
 * @author: myildiz
 * @date: 28.02.2018.
 */
public interface IATVSlotListener {
  /**
   * Please see {@link gov.tubitak.eu.lightest.minderadapters.ATVAdapter#sendDatagramResponse}
   * @param datagramPacket
   */
  void sendDatagramResponse(byte[] datagramPacket);

  /**
   * Please see {@link gov.tubitak.eu.lightest.minderadapters.ATVAdapter#sendTestAssetResponse}
   * @param assetData
   */
  void sendTestAssetResponse(byte[] assetData);
}
