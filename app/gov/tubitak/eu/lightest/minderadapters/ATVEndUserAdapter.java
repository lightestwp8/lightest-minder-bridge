package gov.tubitak.eu.lightest.minderadapters;

import gov.tubitak.eu.common.InitialTestData;
import gov.tubitak.eu.common.TestResult;
import java.util.ArrayList;
import java.util.List;
import minderengine.Signal;
import minderengine.Slot;

/**
 * @author: myildiz
 * @date: 26.02.2018.
 */
public abstract class ATVEndUserAdapter extends LIGHTestMinderAdapter {

  //<editor-fold desc="ATV End User SLOT LISTENER Registration">

  private List<IATVEndUserSlotListener> slotListeners = new ArrayList<>();

  public void registerATBSlotListener(IATVEndUserSlotListener listener) {
    this.slotListeners.add(listener);
  }

  public void removeATBSlotListener(IATVEndUserSlotListener listener) {
    this.slotListeners.remove(listener);
  }

  //</editor-fold>

  /**
   * Sends a trigger to the ATV-MOC to start the test
   */
  @Slot
  public void triggerTest(InitialTestData initialTestData) {
    for (IATVEndUserSlotListener listener : slotListeners) {
      listener.triggerTest(initialTestData);
    }
  }

  /**
   * The signal that is triggered by the client when the test is done
   *
   * @param result the container of the test resuls
   */
  @Signal
  public abstract void testFinished(TestResult result);
}
