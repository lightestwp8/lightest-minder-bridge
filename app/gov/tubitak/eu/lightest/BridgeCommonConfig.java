package gov.tubitak.eu.lightest;

import java.io.IOException;
import java.net.URL;
import java.util.Properties;

/**
 * @author: myildiz
 * @date: 1.03.2018.
 */
public class BridgeCommonConfig {

  private static URL atvTriggerUrl;
  private static int dnsProxyPort;
  /**
   * true if the trust asset http request should be forwarded to the MINDER signal, false if the asset should be sought in the /trust/
   * resource directory
   */
  private static boolean trustAssetRequestForwarded;


  /**
   * The maximum timeout to make a request for a trust xml wait.
   */
  private static long trustAssetRequestWaitTimeout;


  /**
   * Timeout for waiting for the dns response
   */
  private static long dnsQueryWaitTimeout;

  static {
    Properties properties = new Properties();
    try {
      properties.load(BridgeCommonConfig.class.getResourceAsStream("/bridgeConf/bridge.properties"));

      BridgeCommonConfig.atvTriggerUrl = new URL(properties.getProperty("atv.moc.trigger.port"));
      BridgeCommonConfig.dnsProxyPort = Integer.parseInt(properties.getProperty("dns.proxy.port"));
      BridgeCommonConfig.trustAssetRequestForwarded = Boolean.parseBoolean(properties.getProperty("trust.asset.request.forwarded"));
      BridgeCommonConfig.trustAssetRequestWaitTimeout = Long.parseLong(properties.getProperty("trust.asset.request.wait.timeout"));
      BridgeCommonConfig.dnsQueryWaitTimeout = Long.parseLong(properties.getProperty("dns.query.wait.timeout"));
    } catch (IOException e) {
      throw new IllegalStateException(e);
    }
  }



  public static URL getAtvTriggerUrl() {
    return atvTriggerUrl;
  }


  public static int getDnsProxyPort() {
    return dnsProxyPort;
  }

  public static boolean isTrustAssetRequestForwarded() {
    return trustAssetRequestForwarded;
  }

  public static long getTrustAssetRequestWaitTimeout() {
    return trustAssetRequestWaitTimeout;
  }

  public static long getDnsQueryWaitTimeout() {
    return dnsQueryWaitTimeout;
  }

  public static void setDnsQueryWaitTimeout(long dnsQueryWaitTimeout) {
    BridgeCommonConfig.dnsQueryWaitTimeout = dnsQueryWaitTimeout;
  }
}
