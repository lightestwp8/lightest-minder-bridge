package gov.tubitak.eu.lightest.dns;

import gov.tubitak.eu.lightest.BridgeCommonConfig;
import gov.tubitak.eu.lightest.minderadapters.IATVSlotListener;
import gov.tubitak.eu.lightest.minderadapters.MinderAdapterContainer;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.Arrays;
import javax.inject.Inject;
import javax.inject.Singleton;
import play.Logger;
import play.Logger.ALogger;

@Singleton
public class DnsProxy extends Thread {

  private static final ALogger LOGGER = Logger.of(DnsProxy.class);

  protected DatagramSocket serverSocket = null;
  protected boolean morePackets = true;

  @Inject
  private MinderAdapterContainer minderAdapterContainer;

  public DnsProxy() throws IOException {
    LOGGER.debug("Start DNS Proxy");
    serverSocket = new DatagramSocket(BridgeCommonConfig.getDnsProxyPort());
    start();
  }

  public void run() {
    LOGGER.debug("DNS Porxy listening to the port " + BridgeCommonConfig.getDnsProxyPort());
    //create buffer
    byte[] buffer = new byte[1024];
    while (morePackets) {
      try {
        // receive request
        DatagramPacket packet = new DatagramPacket(buffer, buffer.length);
        serverSocket.receive(packet);

        byte[] actual = new byte[packet.getLength()];
        //received Packet Info
        String receivedPacketInfo = new String(packet.getData(), 0, packet.getLength());
        InetAddress clientAddress = packet.getAddress();
        int port = packet.getPort();
        LOGGER.debug("BRIDGE RECEIVED: " + receivedPacketInfo + " FROM " + clientAddress + ":" + port);

        // signal Minder via ATVAdaper
        minderAdapterContainer.atvAdapter().datagramRequestReceived(Arrays.copyOf(packet.getData(), packet.getLength()));

        //wait for Minder to answer
        final Object[] waiterObject = new Object[1];

        //create an inline slot listener
        IATVSlotListener slotListener = new IATVSlotListener() {
          @Override
          public void sendDatagramResponse(byte[] datagramPacket) {
            synchronized (waiterObject) {
              waiterObject[0] = datagramPacket;
              waiterObject.notify();
            }
          }

          @Override
          public void sendTestAssetResponse(byte[] assetData) {
          }
        };

        try {
          minderAdapterContainer.atvAdapter().registerATVSlotListener(slotListener);

          synchronized (waiterObject) {
            try {
              waiterObject.wait(BridgeCommonConfig.getDnsQueryWaitTimeout());
            } catch (InterruptedException e) {
              LOGGER.warn("Wait interrupted, send back an error");
            }
          }

          final Object result = waiterObject[0];
          if (result == null) {
            LOGGER.warn("Coudln't receive a dns response within the timeout.");
            //FIXME: this will crash EDONA's api ;-)
            packet = new DatagramPacket("Error".getBytes(), 0, 5);
          } else {
            LOGGER.debug("A datagram packet has been received. Send it back to the client");
            byte[] ref = (byte[]) result;
            packet.setData(ref);
          }
        } finally {
          minderAdapterContainer.atvAdapter().removeATVSlotListener(slotListener);
        }

        LOGGER.debug("packet " + packet.getLength());
        serverSocket.send(packet);
      } catch (IOException e) {
        LOGGER.error(e.getMessage(), e);
        morePackets = false;
      }

    }
    serverSocket.close();
  }

}
