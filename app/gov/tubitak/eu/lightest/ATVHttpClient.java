package gov.tubitak.eu.lightest;

import gov.tubitak.eu.common.InitialTestData;
import gov.tubitak.eu.http.HttpClient;
import gov.tubitak.eu.lightest.minderadapters.IATVEndUserSlotListener;
import gov.tubitak.eu.lightest.minderadapters.MinderAdapterContainer;
import java.util.Arrays;
import javax.annotation.Nonnull;
import javax.inject.Inject;
import javax.inject.Singleton;
import play.Logger;
import play.Logger.ALogger;

/**
 * Sends requests to the ATV Moc
 *
 * @author myildiz
 */
@Singleton
public class ATVHttpClient implements IATVEndUserSlotListener {

  private static final ALogger LOGGER = Logger.of(ATVHttpClient.class);

  private MinderAdapterContainer minderAdapterContainer;

  public ATVHttpClient() {
    //register me as a slot listener to receive slot calls from minder
    LOGGER.info("Create ATVHttpClient");
  }

  @Inject
  public void setMinderAdapterContainer(MinderAdapterContainer minderAdapterContainer) {
    LOGGER.error("Register http client to the minder atv adapter.");
    this.minderAdapterContainer = minderAdapterContainer;
    minderAdapterContainer.atvEndUserAdapter().registerATBSlotListener(ATVHttpClient.this);
  }

  /**
   * Please see {@link gov.tubitak.eu.lightest.minderadapters.ATVEndUserAdapter#triggerTest}
   */
  @Override
  public void triggerTest(@Nonnull InitialTestData initialTestData) {
    LOGGER.debug("Received a trigger. Send it to the ATV Moc " + BridgeCommonConfig.getAtvTriggerUrl());
    LOGGER.debug("Transaction length: " + initialTestData.getTransaction().length);
    LOGGER.debug("policyXML length: " + initialTestData.getPolicy().length);

    try {
      HttpClient.httpPOSTKVP(BridgeCommonConfig.getAtvTriggerUrl(), Arrays.asList(
          //transaction
          initialTestData.getTransaction(),
          //getPolicy
          initialTestData.getPolicy()
      ));
    } catch (Exception ex) {
      Logger.error(ex.getMessage(), ex);
      throw new IllegalStateException(ex.getMessage(), ex);
    }
  }
}
