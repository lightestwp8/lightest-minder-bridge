import com.google.inject.AbstractModule
import gov.tubitak.eu.lightest.ATVHttpClient
import gov.tubitak.eu.lightest.dns.DnsProxy
import gov.tubitak.eu.lightest.minderadapters.MinderAdapterContainer

/**
  * This class is a Guice module that tells Guice how to bind several
  * different types. This Guice module is created when the Play
  * application starts.
  *
  * Play will automatically use any class called `Module` that is in
  * the root package. You can create modules in other locations by
  * adding `play.modules.enabled` settings to the `application.conf`
  * configuration file.
  */
class Module extends AbstractModule {

  override def configure() = {
    bind(classOf[MinderAdapterContainer]).asEagerSingleton()
    bind(classOf[ATVHttpClient]).asEagerSingleton()
    bind(classOf[DnsProxy]).asEagerSingleton()
  }
}
