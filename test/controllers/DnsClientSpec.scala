
import java.net.{DatagramPacket, DatagramSocket, InetSocketAddress}

import gov.tubitak.eu.dns.DatagramParser
import org.scalatestplus.play._
import org.scalatestplus.play.guice._
import play.api.test._

package controllers {

  /**
    * Add your spec here.
    * You can mock out a whole application including requests, plugins etc.
    *
    * For more information, see https://www.playframework.com/documentation/latest/ScalaTestingWithScalaTest
    */
  class DnsClientSpec extends PlaySpec with GuiceOneAppPerTest with Injecting {

    "DnsClientSpec" should {
      "send a datagram packet to the server" in {
        val client = new DatagramSocket()
        client.connect(new InetSocketAddress("localhost", 9988))
        val edonaBytes = "EDONA".getBytes
        client.send(new DatagramPacket(edonaBytes, 0, edonaBytes.length))

        val buffer = new Array[Byte](100);
        val response = new DatagramPacket(buffer, buffer.length)
        client.receive(response)

        println("CLIENT RECEIVED " + new String(response.getData))
      }


      "do bla" in {
        var datagramPacket = new DatagramParser()
      }
    }
  }

}
